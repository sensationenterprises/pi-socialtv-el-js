var instagram_data=[], instagram_data_ids=[], slides_already_visited=[], ele_current_section;

var slideTimeout;

// Random Color (From ConFig)
function random_color() {
	return socialtv_config.slide_bg_colors[Math.floor((Math.random() * socialtv_config.slide_bg_colors.length) )];
}

// Builds FullPageJs SLIDER
function build_slides() {
	$('#hello-world').remove();
	
	var ele_fullpagejs=$('<div></div>').attr({
		id: 'fullpagejs',
	});
	
	ele_current_section=$('<div></div>').addClass('section');
	
	for (d=0; d < instagram_data.length; d++) {
		ele_current_section.append(
			$('<div></div>').addClass('slide').html(
				$('<div></div>').addClass('inner').append(
					$('<article></article>').html(
						$('<img></img>').attr('src', instagram_data[d].image)
					),
					
					$('<aside></aside>').append(
						$('<h2></h2>').addClass('instagram-username').html('@'+instagram_data[d].username),
						$('<img></img>').addClass('company-logo').attr('src', socialtv_config.logo)
					),
					
					$('<div></div>').css('clear', 'both')
				)
			)
		);
		
		ele_fullpagejs.append(ele_current_section);		
	}
		
	$('body').append(ele_fullpagejs);
			
	ele_fullpagejs.fullpage({
		scrollingSpeed: fpj_config.slide_animation_speed*1000,
		
		keyboardScrolling: false,
		slidesNavigation: false,
		recordHistory: false,
		
		css3: true,
		scrollbar: false,
		
		verticalCentered: true,
		paddingTop: '0px',
		paddingBottom: '0px',
		resize: true,
		
		controlArrows: false,
		
		afterRender: function () {
			//on page load, start the slideshow
			slideTimeout = setInterval(function () {
				$.fn.fullpage.moveSlideRight();
			}, fpj_config.change_slide_after*1000);
		},

		onLeave: function (index, direction) {
			//after leaving section 1 (home) and going anywhere else, whether scrolling down to next section or clicking a nav link, this SHOULD stop the slideshow and allow you to navigate the site...but it does not
			if (index == '1') {
				console.log('on leaving the slideshow/section1');
				clearInterval(slideTimeout);
			}
		}
	});	
}

$(document).ready(function() {
	var LoadLogo=new Image();
		LoadLogo.src=socialtv_config.logo;
	
	// Gather Instagram Data For Each Hash Tag
	for (instagram_tag = 0; instagram_tag < socialtv_config['tags_to_search_for'].length; instagram_tag++) {
		$.ajax({
			type: 'GET',
			dataType: "jsonp",
			cache: false,
			
			url: 'https://api.instagram.com/v1/tags/'+ socialtv_config['tags_to_search_for'][instagram_tag] +'/media/recent',
			
			data: {
				'client_id' : socialtv_config.instagram.client_id,
				'count' : 200,
			},
			
			success: function(response) {
				console.log(response);
				
				for (d=0; d < response.data.length; d++) {
					if (
						response.data[d].type == 'image'
						&&
						! ($.inArray(response.data[d].id, instagram_data_ids) !== -1 )
					) {
						instagram_data_ids.push(response.data[d].id);
						
						instagram_data.push({
							image: response.data[d].images.standard_resolution.url,
							username: response.data[d].user.username,
						});
						
						var load_img=new Image();
							load_img.src=response.data[d].images.standard_resolution.url;
					}
				}
			},
		});		
	}
	
	// Hello World	
	var ele_hello_world_counter=$('#hello-world-counter');
	
	ele_hello_world_counter.css('background-color', random_color()).text(hello_world_count_to);
	
	hello_world_countdown=setInterval(function() {
		var current_val=Number(ele_hello_world_counter.text());
		
		if (current_val == 1) {
			clearInterval(hello_world_countdown);
			
			build_slides();
		}
		else {		
			ele_hello_world_counter.css('background-color', random_color()).text((current_val-1));
		}
	}, 1000);
	
	
	// Reload Window After
	setTimeout(function() {location.reload();}, (socialtv_config.reload_window_after*(60*1000)));
});



